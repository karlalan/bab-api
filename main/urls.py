from rest_framework import routers
from .views import BaBStaffViewSet

router = routers.DefaultRouter()
router.register('users/staff', BaBStaffViewSet)