from django.shortcuts import render
import django_filters
from rest_framework import viewsets, filters
from .models import Staff
from .serializers import StaffSerializer
from rest_framework.decorators import api_view


class BaBStaffViewSet(viewsets.ModelViewSet):
	queryset = Staff.objects.all().order_by('-account')
	serializer_class = StaffSerializer


# **** for not being used as rest api
# from django.views import View
# import django.http


# **** for not being used as rest api
# class BaBHome(View):
# 	"""Top page
# 	View: object
# 		django view object
# 	"""

# 	def get(self, request):
# 		"""Get method
# 		request: object
# 			django http request
# 		"""
# 		data = {}
# 		return render(request, "index.html", data)