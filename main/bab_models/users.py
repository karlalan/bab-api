from django.db import models

class Staff(models.Model):
	"""BaB Staff: Admin model

	"""
	account    = models.CharField(max_length=100)
	password   = models.CharField(max_length=100)
	email      = models.EmailField()
	authority  = models.PositiveIntegerField(default=1)
	updated_at = models.DateTimeField(auto_now=True)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		indexes = [
			models.Index(fields=['account'], name="staff_account_idx"),
			models.Index(fields=['authority'], name="authority_idx")
		]


class Customers(models.Model):
	"""BaB Customers: User model

	"""
	account    = models.CharField(max_length=100)
	password   = models.CharField(max_length=100)
	email      = models.EmailField()
	updated_at = models.DateTimeField(auto_now=True)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		indexes = [
			models.Index(fields=['account'], name="customers_account_idx")
		]