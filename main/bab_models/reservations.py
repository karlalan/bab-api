from django.db import models
from .users import Staff, Customers

class Rooms(models.Model):
	"""Bab Rooms: Admin's model

	"""
	type         = models.PositiveIntegerField(default=0)
	numbers      = models.PositiveIntegerField(default=0)
	has_room     = models.BooleanField(default=False)
	# image files will be saved to MEDIA_URL/rooms/
	image        = models.ImageField(upload_to='rooms/', height_field='image_height', width_field='image_width')
	image_height = models.PositiveIntegerField(default=0)
	image_width  = models.PositiveIntegerField(default=0)
	descriptions = models.CharField(max_length=1000, blank=True)
	uploaded_by  = models.ForeignKey(Staff, on_delete=models.CASCADE)
	updated_at   = models.DateTimeField(auto_now=True)
	created_at   = models.DateTimeField(auto_now_add=True)

	def __str__(self) -> str:
		return self.id

	class Meta:
		indexes = [
			models.Index(fields=['type'], name="room_type_idx"),
			models.Index(fields=['numbers'], name="room_numbers_idx"),
			models.Index(fields=['has_room'], name="has_room_idx")
		]


class Reservations(models.Model):
	"""BaB Reservations: User's model

	"""
	reserved_by  = models.ForeignKey(Customers, on_delete=models.CASCADE)
	room_type    = models.IntegerField()
	check_in_at  = models.DateTimeField()
	check_out_at = models.DateTimeField()
	updated_at   = models.DateTimeField(auto_now=True)
	created_at   = models.DateTimeField(auto_now_add=True)

	def __str__(self) -> str:
		return self.id

	class Meta:
		indexes = [
			models.Index(fields=['reserved_by'], name="reserved_by_idx"),
			models.Index(fields=['room_type'], name="reservation_room_type_idx"),
			models.Index(fields=['check_in_at'], name="check_in_at_idx"),
			models.Index(fields=['check_out_at'], name="check_out_at_idx"),
		]